# README #

This README would normally document whatever steps are necessary to get your application up and running.

This application provide detail of Rubik’s Cube


* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
* Summary of set up


git clone git@bitbucket.org:imosudi/rubiks.git

cd rubiks

virtualenv venv

source venv/bin/activate

pip install -r requirements.txt

Install tkinter and python-dev

Ubuntu, Debian and other Debian based distros

	sudo apt-get install python-tk python-dev -y 

python app.py runserver

access from web browser

http://127.0.0.1:5000


* Configuration
* Dependencies
certifi==2017.7.27.1
chardet==3.0.4
click==6.7
cycler==0.10.0
dominate==2.3.1
Flask==0.12.2
Flask-Bootstrap==3.3.7.1
Flask-Moment==0.5.1
Flask-Script==2.0.5
Flask-WTF==0.14.2
functools32==3.2.3.post2
idna==2.6
itsdangerous==0.24
Jinja2==2.9.6
MarkupSafe==1.0
matplotlib==2.0.2
numpy==1.13.1
panda==0.3.1
pandas==0.20.3
pyparsing==2.2.0
python-dateutil==2.6.1
pytz==2017.2
requests==2.18.4
six==1.10.0
subprocess32==3.2.7
urllib3==1.22
visitor==0.1.3
Werkzeug==0.12.2
WTForms==2.1

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
Mosudi Isiaka
imosudi@gmail.com

* Other community or team contact


REFERENCES 
#ref: https://github.com/davidwhogg/MagicCube
#ref: https://stackoverflow.com/questions/646286/python-pil-how-to-write-png-image-to-string
#ref: https://stackoverflow.com/questions/25140826/generate-image-embed-in-flask-with-a-data-uri
#ref: https://stackoverflow.com/questions/34492197/how-to-render-and-return-plot-to-view-in-flask
#ref: http://jinja.pocoo.org/docs/dev/templates/#include
#ref: https://github.com/miguelgrinberg/flasky/issues/79
